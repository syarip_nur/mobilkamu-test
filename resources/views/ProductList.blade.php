@extends('layout.sidebarmenu')

@section('title')
	    <title>Product List</title>
	    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    
@endsection

@section('content')
	<table id="example1"  class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Variant</th>
                <th>Tahun</th>
                <th>Price</th>
                <th>Fuel</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tbody>
	      @if(count($products) > 0)
	          @foreach($products->all() as $product)
	            <tr>
	                <td>{{ $product->id}}</td>
	                <td>{{ $product->brand }}</td>
	                <td>{{ $product->model }}</td>
	                <td>{{ $product->variant }}</td>
	                <td>{{ $product->tahun }}</td>
	                <td>{{ $product->price }}</td>
	                <td>{{ $product->fuel }}</td>
	                <td>
	                  <center>
	                     <a href='{{ url("/product-detail/{$product->id}") }}' class="btn btn-sm btn-primary"><i class="fas fa-folder-open"></i></a>
	                  </center>
	                </td>
	            </tr>
	          @endforeach
	      @endif
	    </tbody>
    </table>
@endsection

@section('javascript')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js
"></script>
<script>

$(document).ready(function() {
    $('#example1').DataTable();
} );
</script>
@endsection