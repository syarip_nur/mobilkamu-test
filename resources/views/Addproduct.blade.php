@extends('layout.sidebarmenu')

@section('title')
	    <title>Add Product</title>	    
@endsection

@section('content')
<div class="container-fluid bg-light navbar-light">
	<br>
		<H3>Form Input Product</H3>
	<br>
<form id="form-konten" action="{{ url('/save') }}" method="POST"  enctype="multipart/form-data"r>
	<div class="form-row">
		<div class="form-group col-md-6">
	      	<label for="exampleInputEmail1">Brand</label>
		    	<select name="brand" class="form-control form-control-sm select2" placeholder="Pilih Brand" style="width: 100%;" required>
			   		<option value="">Choose</option>
	            	@foreach ($brands as $brand)
	                    <option value="{{ $brand->id }}">{{ $brand->brand }}</option>
	                @endforeach
			    </select>
	    </div>
	    <div class="form-group col-md-6">
	    	<label for="exampleInputEmail1">Model</label>
		    	<select name="model" class="form-control form-control-sm select2" placeholder="Pilih Model" style="width: 100%;" required>
			   		<option value="">Choose</option>
	            	@foreach ($models as $model)
	                    <option value="{{ $model->id }}">{{ $model->model }}</option>
	                @endforeach
			    </select>	
	    </div>
	</div>	

	<div class="form-group">
	    <label for="exampleInputEmail1">Variant</label>
	    	<select name="variant" class="form-control form-control-sm select2" style="width: 100%;" required>
		   		<option value="">Choose</option>
            	@foreach ($variants as $variant)
                    <option value="{{ $variant->id }}">{{ $variant->variant }}</option>
                @endforeach
		    </select>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="exampleInputEmail1">Tahun</label>
			<select name="tahun" class="form-control form-control-sm select2" style="width: 100%;" required>
		   		<option value="">Pilih tahun</option>
            	@foreach ($tahuns as $tahun)
                    <option value="{{ $tahun }}">{{ $tahun }}</option>
                @endforeach
		    </select>
		</div>
		<div class="form-group col-md-8">
			<label>Fuel</label>
			<select  class="form-control form-control-sm" name="fuel" required>
		        <option selected>Choose...</option>
		        <option>Bensin</option>
		        <option>Solar</option>
		        <option>Listrik</option>
		    </select>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-7">
			<label for="exampleInputEmail1">Price</label>
			<input type="text" name="price" id="rupiah" class="form-control form-control-sm" required>
		</div>
		<div class="form-group col-md-5">
			<label>Stock</label>
			<input type="text" name="stock" id="stock" class="form-control form-control-sm" required>
		</div>
	</div>

	<div class="form-group row">
	    <div class="col-sm-2">Color</div>
	    <div class="col-sm-2">
	      <div class="form-check">
	        <input name="color[]" class="form-check-input" type="checkbox" id="gridCheck1" value="merah">
	        <label class="form-check-label" for="gridCheck1" style="color: #e81a13;">
	          Merah
	        </label>
	      </div>
	    </div>
	    <div class="col-sm-2">
	      <div class="form-check">
	        <input name="color[]" class="form-check-input" type="checkbox" id="gridCheck1" value="hitam">
	        <label class="form-check-label" for="gridCheck1">
	          Hitam
	        </label>
	      </div>
	    </div>
	    <div class="col-sm-2">
	      <div class="form-check">
	        <input name="color[]" class="form-check-input" type="checkbox" id="gridCheck1" value="biru">
	        <label class="form-check-label" for="gridCheck1" style="color: #1325e8;">
	          Biru
	        </label>
	      </div>
	    </div>
	    <div class="col-sm-2">
	      <div class="form-check">
	        <input name="color[]" class="form-check-input" type="checkbox" id="gridCheck1" value="kuning">
	        <label class="form-check-label" for="gridCheck1" style="color: #dde813;">
	          Kuning
	        </label>
	      </div>
	    </div>
	</div>

	<div class="form-row">
		<div class="col-md-5">
			<label for="exampleFormControlFile1">Input Image</label>
    		<input type="file" name="image" class="form-control-file form-control-sm" id="exampleFormControlFile1" required>
		</div>
	</div>
	 <input type='hidden' name='_token' value='{{ csrf_token() }}'>
	<br>
	<br>
	<center>
		<button class="btn btn-primary btn-sm">
          <i class="fas fa-paper-plane"></i> Save
        </button>
        <a href="{{url('/')}}" class="btn btn-danger btn-sm"><i class="fas fa-sync"></i> Reset</a>
	</center>
	<br>
	<br>
		
</form>
</div>
	
@endsection

@section('javascript')
  	<!-- Select2 -->
    <script src="{{ asset('/js/select2.js')}}"></script>
    <script type="text/javascript">
        $(function () {
        //Initialize Select2 Elements
            $('.select2').select2()
        });

        var rupiah = document.getElementById("rupiah");
		rupiah.addEventListener("keyup", function(e) {
		  // tambahkan 'Rp.' pada saat form di ketik
		  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
		  rupiah.value = formatRupiah(this.value, "Rp. ");
		});


		var stock = document.getElementById("stock");
		stock.addEventListener("keyup", function(e) {
		  // tambahkan 'Rp.' pada saat form di ketik
		  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
		  stock.value = formatRupiah(this.value);
		});

		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix) {
		  var number_string = angka.replace(/[^,\d]/g, "").toString(),
		    split = number_string.split(","),
		    sisa = split[0].length % 3,
		    rupiah = split[0].substr(0, sisa),
		    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		  // tambahkan titik jika yang di input sudah menjadi angka ribuan
		  if (ribuan) {
		    separator = sisa ? "." : "";
		    rupiah += separator + ribuan.join(".");
		  }

		  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
		  return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
		}
    </script>
@endsection
