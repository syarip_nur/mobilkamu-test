@extends('layout.sidebarmenu')

@section('title')
	    <title>Detail Product</title>	    
@endsection

@section('content')
<div class="container-fluid bg-light navbar-light">
	<div class="form-row">
		<div class="form-group col-md-5">
			<h4>{{ $products->brand }} {{ $products->model }} {{ $products->variant }}</h4>
			<img src="{{ $image }}" class="img-fluid" alt="Responsive image">
		</div>
		<div class="form-group col-md-6" style="margin-left: 30px;">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="exampleInputEmail1">Brand</label>
					<input type="text" name="brand" id="rupiah" class="form-control form-control-sm" readonly="" value="{{ $products->brand}}">
				</div>
				<div class="form-group col-md-6">
					<label for="exampleInputEmail1">Model</label>
					<input type="text" name="model" id="rupiah" class="form-control form-control-sm" readonly="" value="{{ $products->model}}">
				</div>
				<div class="form-group col-md-8">
					<label for="exampleInputEmail1">Variant</label>
					<input type="text" name="variant" id="rupiah" class="form-control form-control-sm" readonly="" value="{{ $products->variant}}">
				</div>
				<div class="form-group col-md-4">
					<label for="exampleInputEmail1">Tahun</label>
					<input type="text" name="variant" id="rupiah" class="form-control form-control-sm" readonly="" value="{{ $products->tahun}}">
				</div>
				<div class="form-group col-md-6">
					<label for="exampleInputEmail1">Fuel</label>
					<input type="text" name="fuel" id="rupiah" class="form-control form-control-sm" readonly="" value="{{ $products->fuel}}">
				</div>
				<div class="form-group col-md-6">
					<label for="exampleInputEmail1">Price</label>
					<input type="text" name="price" id="rupiah" class="form-control form-control-sm" readonly="" value="<?php echo  number_format($products->price,2,',','.');?>">
				</div>
				<div class="form-group col-md-3">
					<label for="exampleInputEmail1">Stock</label>
					<input type="text" name="stock" id="rupiah" class="form-control form-control-sm" readonly="" value="<?php echo  number_format($products->stok);?>">
				</div>
				<div class="form-group col-md-9">
					<label for="exampleInputEmail1">Color</label>
					<input type="text" name="color" id="rupiah" class="form-control form-control-sm" readonly="" value=" {{$colors}}">
				</div>					
			</div>	
		</div>
	</div>	
</div>	
@endsection