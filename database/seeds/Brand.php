<?php

use Illuminate\Database\Seeder;
use App\Models\Brands;

class Brand extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
        	['brand' => 'Honda'],
        	['brand' => 'Toyota'],
        	['brand' => 'Suzuki'],
        	['brand' => 'Nissan']
        ];

        foreach($brands as $brand){
		    Brands::create($brand);
		}
    }
}
