<?php

use Illuminate\Database\Seeder;
use App\Models\CarModels;

class Model extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = [
        	['model' => 'Jazz'],
        	['model' => 'Mobilio'],
        	['model' => 'Avanza'],
        	['model' => 'Alvhard'],
        	['model' => 'APV'],
        	['model' => 'Baleno'],
        	['model' => 'Juke'],
        ];

        foreach ($models as $model) {
        	CarModels::create($model);
        }
    }
}
