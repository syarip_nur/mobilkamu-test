<?php

use Illuminate\Database\Seeder;
use App\Models\Variants;

class Variant extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variants = [
        	['variant' => 'RS'],
        	['variant' => 'S'],
        	['variant' => 'E'],
        	['variant' => '1.3 E'],
        	['variant' => '1.3 G'],
        	['variant' => '2.5 G'],
        	['variant' => '2.5 X'],
        	['variant' => 'GA'],
        	['variant' => 'GX'],
        	['variant' => 'GL'],
        	['variant' => '1.5 CVT Grey Interior'],
        	['variant' => '1.5 Revolt CVT Grey Interior']
        ];

        foreach($variants as $variant){
        	Variants::create($variant);
        }
    }
}
