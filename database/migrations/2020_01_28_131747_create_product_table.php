<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('brand_id')->length(4)->unsigned();
            $table->integer('model_id')->length(4)->unsigned();
            $table->integer('variant_id')->length(4)->unsigned();
            $table->integer('tahun')->length(4)->unsigned();
            $table->string('fuel', 150);
            $table->integer('price')->length(11)->unsigned();
            $table->integer('stok')->length(4)->unsigned();
            $table->string('color', 200);
            $table->string('image', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
