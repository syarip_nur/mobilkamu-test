<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Product')->group(function () {
	Route::get('/', 'ProductController@form');
	Route::post('/save', 'ProductController@save');
	Route::get('/product-detail/{id}', 'ProductController@detail');
	Route::get('/product', 'ProductController@showAll');   	
});

