## Info

- PHP version 7.2.
- Laravel Version 5.8
- Mysql
- Bootstrap
- jQuery

## Deployment Step

- Clone Project 
- Composer install
- Create Database dengan nama database mobilkamu
- Copy env.example dan rename menjadi .env
- Di file .env ubah koneksi database sesuai dengan kebutuhan
- Jalankan perintah (php artisan migrate) di terminal, untuk otomatis membuat table
- Jalankan perintah (php artisan db:seed) di terminal, untuk otomatis mengisi data dummy
- Project siap digunakan.
 


