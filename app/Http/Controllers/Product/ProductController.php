<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brands;
use App\Models\CarModels;
use App\Models\Variants;
use App\Models\Products;
use Yajra\Datatables\Datatables;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ProductController extends Controller
{

    public function form()
    {    
  		$brands 		= Brands::orderBy('brand', 'ASC')->get();
  		$models 		= CarModels::orderBy('model', 'ASC')->get();
  		$variants 		= Variants::orderBy('variant', 'ASC')->get();
  		$tahunSekarnag 	= date('Y');
  		
  		for($t = 1990; $t<$tahunSekarnag; $t++)
  		{
  			$tahuns[] = $t;
  		}

        return view('Addproduct', ['brands' => $brands, 'models' => $models, 
        	'variants' => $variants, 'tahuns' => $tahuns]);      
    	 
    }

    public function save(Request $request)
    {
    	try {
    		
    		$brand 				= $request->input('brand');
    		$model 				= $request->input('model');
    		$variant 			= $request->input('variant');
    		$tahun 				= $request->input('tahun');
    		$fuel 				= $request->input('fuel');
    		$price 				= preg_replace("/[^0-9]/", "", $request->input('price'));
    		$stock 				= preg_replace("/[^0-9]/", "", $request->input('stock'));
    		$color 				= $request->input('color');

    		//upload foto
			if($request->hasFile('image'))
			{
               	$file 		= $request->file('image');
               	$nama_file 	= time()."_".$file->getClientOriginalName();
               	$tujuan_upload = public_path().'/image';
               	$file->move($tujuan_upload,$nama_file);
            }else{
            	$nama_file ="";
            }

            $simpan = Products::insertGetId([
				'brand_id' 		=> $brand,
				'model_id' 		=> $model,
				'variant_id' 	=> $variant,
				'tahun' 		=> $tahun,
				'fuel' 			=> $fuel,
				'price' 		=> $price,
				'stok' 			=> $stock,
				'color' 		=> json_encode($color),
				'image' 		=> $nama_file,
				'created_at' 	=> date('Y-m-d H:i:s'),
				'updated_at' 	=> date('Y-m-d H:i:s')
			]);

			return redirect('/product-detail/'.$simpan);

    	} catch (Exception $e) {
    		return redirect('/');
    	}
    }

    public function detail($id)
    {

    	$Products = Products::leftjoin('brand', 
            	'product.brand_id', '=','brand.id')
    		->leftjoin('model', 
    			'product.model_id', '=', 'model.id')
    		->leftjoin('variant', 
    			'product.variant_id', '=', 'variant.id')
    		->where('product.id', $id)->first();
    		
    	if(empty($Products))
    	{
    		return redirect('/');
    	}

    	$colors 	= implode(", ",json_decode($Products->color));
    	$image 		= asset('image').'/'.$Products->image;

    	return view('DetailProduct', ['products' => $Products, 'colors' => $colors, 'image' => $image]);
    }


    public function showAll()
    {	
    	$products = Products::leftjoin('brand', 
            	'product.brand_id', '=','brand.id')
    		->leftjoin('model', 
    			'product.model_id', '=', 'model.id')
    		->leftjoin('variant', 
    			'product.variant_id', '=', 'variant.id')
    		->select('product.id', 'brand.brand', 'model.model',
    			'variant.variant', 'product.tahun', 'product.price',
    			'product.fuel')->get();
    	return view('ProductList', ['products' => $products]);
    }
}
