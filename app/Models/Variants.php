<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variants extends Model
{
    protected $table = 'variant';
    protected $guarded = ['id'];
        
}
